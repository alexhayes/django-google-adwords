=====================
django-google-adwords
=====================

This repository, along with many improvements, has moved to `github.com/alexhayes/django-google-adwords`_ and will be deleted in a few weeks.

.. _`github.com/alexhayes/django-google-adwords`: https://github.com/alexhayes/django-google-adwords